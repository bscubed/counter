package com.genesys.counter

class Counter(private val time: Long, private val interval: Long, private val counterUpdateListener: CounterUpdateListener) : Runnable {

    /**
     * Whether or not this counter instance is currently running. It can also be used to stop the
     * counter if it's running.
     */
    var alive = false

    /**
     * Inherited from [Runnable.run]
     * It's executed when a [Thread] accepts this object as a parameter, and [Thread.start] is triggered.
     * It is overridden here to call [CounterUpdateListener.counterUpdated] at a constant, specific interval for
     * a designated amount of time. When finished, it calls [CounterUpdateListener.counterFinished].
     */
    override fun run() {
        val startTime = System.currentTimeMillis()
        var nextInterval = startTime + interval
        val finishTime = startTime + time
        alive = true

        while (System.currentTimeMillis() < finishTime && alive) {
            if (System.currentTimeMillis() > nextInterval) {
                counterUpdate(startTime, finishTime)
                nextInterval += interval
            }
        }

        counterUpdate(startTime, finishTime)

        counterUpdateListener.counterFinished()
    }

    /**
     * Stops the counter if it is currently running.
     */
    fun stop() {
        alive = false
    }

    /**
     * Calculates both the amount of time the counter has left in milliseconds, and the completion percent as a double.
     * @param startTime: The time when the counter started.
     * @param finishTime: The time when the timer will end.
     */
    private fun counterUpdate(startTime: Long, finishTime: Long) {
        val timeLeft = finishTime - System.currentTimeMillis()
        val percent: Double = (System.currentTimeMillis() - startTime).toDouble() / time.toDouble()
        counterUpdateListener.counterUpdated(timeLeft, percent)
    }

    /**
     * An interface another class can implement to receive information about the counter
     */
    interface CounterUpdateListener {
        fun counterUpdated(timeLeft: Long, percent: Double)
        fun counterFinished()
    }
}