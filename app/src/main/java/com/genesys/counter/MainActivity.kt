package com.genesys.counter

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.NumberPicker
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), Counter.CounterUpdateListener {

    lateinit var counter: Counter
    private lateinit var startFabListener: View.OnClickListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Formats the second number picker to always have two digits.
        val secondFormatter = NumberPicker.Formatter { value -> String.format("%02d", value) }

        // Formats the minute number picker to eliminate any leading zeros.
        val minuteFormatter = NumberPicker.Formatter { value -> String.format("%2d", value) }

        // Sets the minimum and maximum values for our second number picker.
        second_number_picker.minValue = 0
        second_number_picker.maxValue = 59
        second_number_picker.value = DEFAULT_SECONDS
        second_number_picker.setFormatter(secondFormatter)

        // Sets the minimum and maximum values for our minute number picker and set the default value to one minute.
        minute_number_picker.minValue = 0
        minute_number_picker.maxValue = 59
        minute_number_picker.value = DEFAULT_MINUTES
        minute_number_picker.setFormatter(minuteFormatter)

        // Sets the action to run when clicking the play FloatingActionButton.
        startFabListener = View.OnClickListener {
            val minutes = minute_number_picker.value
            val seconds = second_number_picker.value
            val time = getTimeInMilliseconds(minutes, seconds)

            // Here, we check that the timer isn't set to zero. If it is, the play button gets stuck.
            if (time > 0) {
                // Initialize a new counter object to update our UI every 10 milliseconds, then run it in a thread.
                counter = Counter(time, UPDATE_INTERVAL, this)
                Thread(counter).start()

                // Hides the number pickers and shows our countdown text view.
                time_text_view.visibility = View.VISIBLE
                number_picker_container.visibility = View.GONE

                // Replaces the play button icon with a stop icon.
                start_fab.setImageDrawable(resources.getDrawable(R.drawable.ic_stop, theme))
                start_fab.setOnClickListener {
                    counter.stop()
                }
            }
        }
        resetCounter()
    }

    /**
     * Resets all of our UI components to their default values.
     */
    private fun resetCounter() {
        time_text_view.visibility = View.GONE
        number_picker_container.visibility = View.VISIBLE

        time_progress_bar.progress = time_progress_bar.max
        start_fab.setImageDrawable(resources.getDrawable(R.drawable.ic_start, theme))
        start_fab.setOnClickListener(startFabListener)
    }

    /**
     * This function is inherited from the [Counter.CounterUpdateListener] class. It is called at a constant, set
     * interval until the counter reaches zero, or is stopped.
     * @param timeLeft: The amount of time until the counter reaches zero, in milliseconds.
     */
    override fun counterUpdated(timeLeft: Long, percent: Double) {
        // Updates the progress bar.
        time_progress_bar.progress = (time_progress_bar.max * percent).toInt()

        // TODO: Show TextView instead of number pickers when the timer is active.
        val seconds = Math.ceil(timeLeft.toDouble() / 1000.toDouble()).toInt()
        val time = "${seconds / 60}:${String.format("%02d", (seconds % 60))}"
        runOnUiThread {
            time_text_view.text = time
        }
    }

    /**
     * This function is inherited from the [Counter.CounterUpdateListener] class. It executes when the counter runs
     * out. In this case, it is used to reset our timer to its default state.
     */
    override fun counterFinished() {
        runOnUiThread {
            resetCounter()
        }
    }

    /**
     * Converts minutes and seconds to a length of time in milliseconds.
     * @param minutes: Number of minutes.
     * @param seconds: Number of seconds.
     * @return: Amount of minutes and seconds converted to milliseconds.
     */
    private fun getTimeInMilliseconds(minutes: Int, seconds: Int): Long {
        return (((minutes * 60) + seconds) * 1000).toLong()
    }

    companion object {
        const val DEFAULT_MINUTES = 1
        const val DEFAULT_SECONDS = 0
        const val UPDATE_INTERVAL = 10L
    }
}
